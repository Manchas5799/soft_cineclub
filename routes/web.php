<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// RUTAS PARA CREAR CARGOS
// Route::group(['prefix' => 'admin'], function (){
//     Route::resource('cargos','CargoController');
//     Route::resource('salas','SalaController');
//     Route::resource('generos','GenerosController');
// });

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
Route::resource('panel','CategoriaViewController');
Route::get('peliculas','PeliculasViewController@index');
Route::get('peliculas/{genero}','PeliculasViewController@generoFilter');
Route::get('salas/{id}', 'SalaController@main');
Route::get('asientos/{sala}','SalaController@asientos');