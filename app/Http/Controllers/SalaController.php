<?php

namespace App\Http\Controllers;

use App\Models\Asiento;
use App\Models\Funciones;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

use App\Models\Salas;

class SalaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function main($id)
    {   
        // $data = Salas::all();
        $data = Funciones::where('idpelicula',$id)->with('getSala')->get();
        return view('salas.todas', ['funciones' => $data]);
    }
    public function asientos($sala){
        $data = Asiento::where('salas_idsalas',$sala)->get();
        return view('salas.asientos',['asientos' => $data]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $sala = new Salas;
        $sala->asientos_max = $request->asientos_max;
        $sala->codigo_sala = $request->codigo_sala;
        $sala->nombre_sala = $request->nombre_sala;
        $sala->save();
        return Redirect::to('admin/salas');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    
        //
    }
    /*
     *date the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $sala = Salas::findOrFail($id);
        $sala->asientos_max = $request->asientos_max;
        $sala->codigo_sala = $request->codigo_sala;
        $sala->nombre_sala = $request->nombre_sala;
        $sala->update();
        return Redirect::to('admin/salas');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $sala = Salas::findOrFail($id);
        $sala->delete();
        return Redirect::to('Admin/salas');
    }
}
