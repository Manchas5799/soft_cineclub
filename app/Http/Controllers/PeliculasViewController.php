<?php

namespace App\Http\Controllers;

use App\Models\Funciones;
use App\Models\Pelicula;
use Illuminate\Http\Request;
use DB;

class PeliculasViewController extends Controller
{
    public function generoFilter($genero)
    {
        // $data = collect(Pelicula::where('genero',$genero)->get());
        $data = collect(DB::select("call sp_peliculas_x_genero($genero)"));
        //return view('panel.peliculas', compact('data'));
    }
    public function filterPelicula($id){
        // $sql = "SELECT * FROM FUNCIONES as func inner join peliculas as pel inner join salas as sal WHERE func.idpelicula = $id "
        $data = Funciones::where('idpelicula',$id)->with('getSala')->get();
        return view('panel.funciones', ['funciones' => $data]);
    }
    public function index() {
        $data = Pelicula::with('getProductora','getGenero','getDirector','getProductor')->get();
        return view('panel.peliculas', ['peliculas' => $data]);
    }
}
