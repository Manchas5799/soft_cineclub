<?php

namespace App\Http\Controllers;

use App\Models\GeneroPelicula;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class GenerosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = GeneroPelicula::all();
        return View('generos.index', ['data' => $data]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $cargo = new GeneroPelicula();
        $cargo->nombre = $request->nombre;
        $cargo->save();
        return Redirect::to('/admin/generos');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cargo = GeneroPelicula::find($id);
        $cargo->nombre = $request->nombre;
        $cargo->save();
        return Redirect::to('/admin/generos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cargo = GeneroPelicula::find($id);
        $cargo->delete();
        return Redirect::to('/admin/generos');
    }
}
