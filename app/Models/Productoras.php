<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Productoras extends Model
{
    protected $table = "productoras";
    protected $primaryKey = "idproductora";
    public $timestamps = false;
}
