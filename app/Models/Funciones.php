<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Funciones extends Model
{
    protected $table = "funciones";
    protected $primaryKey = "idfunciones";
    public $timestamps = false;

    public function getSala() {
        // SELECT * FROM SALAS WHERE idsalas=funciones.idsala
        return $this->belongsTo(Salas::class,'idsala');
    }
}
