<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Entradas extends Model
{
    protected $table = "entradas";
    protected $primaryKey = "identradas";
    public $timestamps = false;
}
