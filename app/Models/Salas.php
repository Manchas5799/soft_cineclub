<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Salas extends Model
{
    protected $table = "salas";
    protected $primaryKey = "idsalas";
    public $timestamps = false;
}
