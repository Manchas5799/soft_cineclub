<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Personas extends Model
{
    protected $table = "personas";
    protected $primaryKey = "id_personas";
    public $timestamps = false;
}
