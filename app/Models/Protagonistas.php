<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// use App\Models\Personas;

class Protagonistas extends Model
{
    protected $table = "protagonistas";
    protected $primaryKey = "idprotagonistas";
    public $timestamps = false;
    public function getPersona() {
        return $this->belongsTo(Personas::class,'personas_id_personas');
    }
}
