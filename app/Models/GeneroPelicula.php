<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GeneroPelicula extends Model
{
    protected $table = "genero_peliculas";
    protected $primaryKey = "idgenero_peliculas";
    public $timestamps = false;
}
