<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pelicula extends Model
{
    protected $table = "peliculas";
    protected $primaryKey = "idpeliculas";
    public $timestamps = false;

    public function getFunciones() {
        return $this->hasMany(Funciones::class,'idpelicula');
    }
    public function getProductora() {
        return $this->belongsTo(Productoras::class, 'productora');
    }
    public function getGenero() {
        return $this->belongsTo(GeneroPelicula::class, 'genero');
    }
    public function getDirector() {
        return $this->belongsTo(Personas::class, 'director');
    }
    public function getProductor() {
        return $this->belongsTo(Personas::class, 'productor');
    }
}
