@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-sm-4 ml-auto my-2">
            <button class="btn btn-primary btn-block"  data-toggle="modal" data-target="#modelId" type="button">Agregar Cargo</button>
        </div>
        <div class="col-md-8 col-lg-12 mx-auto">
            <table class="table">
                <thead>
                    <tr>
                        <th class="text-center">#</th>
                        <th class="text-center">Asientos</th>
                        <th class="text-center">Codigo de sala</th>
                        <th class="text-center">Nombre de sala</th>
                        <th class="text-center">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $item)
                        <tr>
                            <td class="text-center">{{$item->idsalas}}</td>
                            <td class="text-center">{{$item->asientos_max}}</td>
                            <td class="text-center">{{$item->codigo_sala}}</td>
                            <td class="text-center">{{$item->nombre_sala}}</td>
                            <td class="text-center">
                                <button class="btn btn-warning" data-toggle="modal" data-target="#model{{$item->idsalas}}" type="button">Editar</button>
                                @include('salas.edit',['item'=>$item])
                            <button type="submit" class="btn btn-danger" form="del{{$item->idsalas}}">Eliminar</button>
                            <form action="/admin/salas/{{$item->idsalas}}" method="POST" id="del{{$item->idsalas}}">
                                @csrf
                                <input type="hidden" name="_method" value="DELETE">
                            </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@include('cargos.crear')
@endsection
