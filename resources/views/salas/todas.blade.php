@extends('layouts.app')
@section('css')
<link rel="stylesheet" href="{{ asset('css/sala.css') }}">
@endsection
<!-- $funciones = [ 
    [
        'idfunciones' => "1",
        "hora_inicio" => "##:##",
        "hora_fin" => "##:##",
        "fecha_inicio" => "",
        "fecha_fin" => "",
        "getSala" => [
            'idsalas' => "1",
            'asientos_max' => "200",
            "codigo_sala" => "fasdf2",
            "nombre_sala" => 2
        ]
    ]
]
    -->
@section('content')
<div class="container">
    <div class="row">
        @forelse($funciones as $funcion )
            <div class="card col-lg-3 col-md-4 col-sm-6 my-2 card-caro">
                <div class="card-body">
                <h5 class="card-title">SALA Nº {{$funcion->getSala->idsalas}}</h5>
                    <p class="card-text">
                        Funcion: {{$funcion->fecha_inicio}}<br>
                        Horario: {{$funcion->hora_inicio}}<br>
                        Asientos Max : {{$funcion->getSala->asientos_max}}<br>
                        Cod : {{$funcion->getSala->codigo_sala}}<br>
                        {{$funcion->getSala->nombre_sala}}
                    </p>
                <a href="/asientos/{{$funcion->getSala->idsalas}}" class="btn btn-primary">Buscar Asientos</a> 
                </div>
            </div>
        @empty
            <h2 class="text-black">OH! POR EL MOMENTO NO HAY FUNCIONES...</h2>
        @endforelse
    </div>
</div>
@endsection