<!-- Modal -->
<div class="modal fade" id="model{{$item->idsalas}}" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
    <form class="modal-content" action="/admin/salas/{{$item->idsalas}}" method="POST">
            @csrf
            <input type="hidden" name="_method" value="PUT">
            <div class="modal-header">
                <h5 class="modal-title">Editar Cargo</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="">Asientos</label>
                <input type="text" name="asientos_max" id="" class="form-control" placeholder="" value="{{$item->asientos_max}}" aria-describedby="helpId">
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-primary">Actualizar</button>
            </div>
        </form>
    </div>
</div>