@extends('layouts.app')
@section('css')
<link rel="stylesheet" href="{{ asset('css/asientos.css') }}">
@endsection
@section('content')
<!--
$asientos = [
    [
        'idasiento' => 1,
        "numero" => "2",
        "salas_idsalas" => 3
    ]

]
-->
<div class="container">
    <div class="row">
        @forelse($asientos as $asiento)
            <div class="card text-center col-sm-4 col-md-3 col-lg-2">
                <div class="card-img-top content-img-round">
                    <img class="card-img-top img-round" src="{{asset('img/chair-solid.svg')}}" alt="">
                </div>
                <div class="card-body">
                <h4 class="card-title">A-{{$asiento->numero}}</h4>
                    <button class="btn btn-primary">Asignar</button> 
                </div>
            </div>
        @empty
            <h2 class="text-black text-center">No hay asientos disponibles</h2>        
        @endforelse
    </div>
</div>
@endsection