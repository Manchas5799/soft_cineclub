@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-sm-4 ml-auto my-2">
            <button class="btn btn-primary btn-block"  data-toggle="modal" data-target="#modelId" type="button">Agregar Genero</button>
        </div>
        <div class="col-md-8 col-lg-12 mx-auto">
            <table class="table">
                <thead>
                    <tr>
                        <th class="text-center">#</th>
                        <th class="text-center">Nombre</th>
                        <th class="text-center">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $item)
                        <tr>
                            <td class="text-center">{{$item->idgenero_peliculas}}</td>
                            <td class="text-center">{{$item->nombre}}</td>
                            <td class="text-center">
                                <button class="btn btn-warning" data-toggle="modal" data-target="#model{{$item->idgenero_peliculas}}" type="button">Editar</button>
                                @include('generos.edit',['item'=>$item])
                            <button type="submit" class="btn btn-danger" form="del{{$item->idgenero_peliculas}}">Eliminar</button>
                            <form action="/admin/generos/{{$item->idgenero_peliculas}}" method="POST" id="del{{$item->idgenero_peliculas}}">
                                @csrf
                                <input type="hidden" name="_method" value="DELETE">
                            </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@include('generos.crear')
@endsection
