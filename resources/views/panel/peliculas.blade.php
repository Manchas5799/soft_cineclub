@extends('layouts.app')
@section('css')
<link rel="stylesheet" href="{{ asset('css/pelicula.css') }}">
@endsection
<!-- $peliculas = [ 
    [
        'idpeliculas' => "1",
        "titulo" => "##:##",
        "titulo_original" => "##:##",
        "anio_produccion" => "",
        "color" => "",
        "duracion" => "",
        "argumento" => "",
        "soporte" => "",
        "apreciacion" => "",
        "portada" => "",
        "getDirector" => [
            "id_personas" => 2,
            "nombre_completo" =>  "lorem2"
        ],
        "getProductor" => [
            "id_personas" => 2,
            "nombre_completo" =>  "lorem2"
        ],
        "getGenero" => [
            "idgenero_peliculas" => 2,
            "nombre" =>  "lorem2"
        ],
        "getProductora" => [
            "idproductora" => 2,
            "nombre_productora" =>  "lorem2"
        ],
    ]
]
    -->
@section('content')
<div id="team" class="pb-5">
    <div class="container">
        <h5 class="section-title h1">PELICULAS</h5>
        <div class="row">
            <!-- Team member -->
            @foreach($peliculas as $pelicula)
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                    <div class="image-flip" >
                        <div class="mainflip flip-0">
                            <div class="frontside">
                                <div class="card">
                                    <div class="card-body text-center">
                                        <img class=" img-fluid" src="{{Voyager::image($pelicula->portada)}}" alt="card image">
                                        <h4 class="card-title">{{$pelicula->titulo}}</h4>
                                        <p class="card-text text-left">
                                        {{$pelicula->anio_produccion}}/{{$pelicula->duracion}}<br>
                                        {{$pelicula->getGenero->nombre}}/{{$pelicula->color}}<br>
                                        Dirigida por {{$pelicula->getDirector->nombre_completo}}<br>
                                        Producida por {{$pelicula->getProductor->nombre_completo}}<br>
                                        Productora: {{$pelicula->getProductora->nombre_productora}}<br>
                                        Apreciación: <br><div class="text-custom">{{$pelicula->apreciacion}}</div>
                                        </p>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="backside">
                                <div class="card">
                                    <div class="card-body text-center mt-4">
                                        <h4 class="card-title">{{$pelicula->titulo}}</h4>
                                        <p class="card-text " style="font-size: 12px">{{$pelicula->argumento}}</p>
                                    <a href="/salas/{{$pelicula->idpeliculas}}" class="btn btn-primary btn-sm text-white">
                                            {{-- <i class="fa fa-plus text-white"></i> --}}
                                            <img src="{{asset('img/popcorn.svg')}}" class="img-icono" alt=""><br>
                                            <span class="text-white">Ver Funciones</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
@endsection